package tr.com.kolaysoft.manav.domain;

import org.junit.jupiter.api.*;
import tr.com.kolaysoft.manav.domain.enumeration.Unit;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class SaleProductTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }
    @Test
    void testGetterSetterId() {
        final Sale sale= new Sale(); sale.setId(2L);
        final Product product= new Product(); product.setId(3L);
        final SaleProductId saleProductId= new SaleProductId();
        saleProductId.setSaleId(sale.getId());
        saleProductId.setProductId(product.getId());
        final SaleProduct saleProduct= new SaleProduct();
        saleProduct.setId(saleProductId);
        assertEquals(saleProduct.getId().getProductId(),saleProductId.getProductId());
        assertEquals(saleProduct.getId().getSaleId(),saleProductId.getSaleId());
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterCount() {
        final BigDecimal count= BigDecimal.valueOf(1345677888);
        final SaleProduct saleProduct= new SaleProduct();
        saleProduct.setCount(BigDecimal.valueOf(1345677888));
        assertEquals(saleProduct.getCount(), count);
        System.out.println("testGetterSetterCount method working");
    }
    @Test
    void testGetterSetterPrice() {
        final BigDecimal price= BigDecimal.valueOf(1345677888);
        final SaleProduct saleProduct= new SaleProduct();
        saleProduct.setPrice(BigDecimal.valueOf(1345677888));
        assertEquals(saleProduct.getPrice(), price);
        System.out.println("testGetterSetterPrice method working");
    }
    @Test
    void testGetterSetterSale() {
        final Sale sale= new Sale(); sale.setId(5L);
        final SaleProduct saleProduct= new SaleProduct();
        saleProduct.setSale(sale);
        assertEquals(saleProduct.getSale(), sale);
        System.out.println("testGetterSetterSale method working");
    }
    @Test
    void testGetterSetterProduct() {
        final Product product= new Product(); product.setId(5L);
        final SaleProduct saleProduct= new SaleProduct();
        saleProduct.setProduct(product);
        assertEquals(saleProduct.getProduct(), product);
        System.out.println("testGetterSetterProduct method working");
    }
    @Test
    @DisplayName("count() method")
    void testMethodCount() {
        final BigDecimal count= BigDecimal.valueOf(1345677888);
        final SaleProduct saleProduct= new SaleProduct();
        saleProduct.count(BigDecimal.valueOf(1345677888));
        assertEquals(saleProduct.getCount(), count);
        System.out.println("testMethodCount method working");
    }
    @Test
    @DisplayName("price() method")
    void testMethodPrice() {
        final BigDecimal price= BigDecimal.valueOf(1345677888);
        final SaleProduct saleProduct= new SaleProduct();
        saleProduct.price(BigDecimal.valueOf(1345677888));
        assertEquals(saleProduct.getPrice(), price);
        System.out.println("testMethodPrice method working");
    }
    @Test
    @DisplayName("sale() method")
    void testMethodSale() {
        final Grocery grocery=new Grocery(); grocery.setName("Erdal bakkal");
        final Sale sale= new Sale(); sale.setId(2L); sale.setGrocery(grocery);
        final SaleProduct saleProduct= new SaleProduct();
        saleProduct.sale(sale);
        assertEquals(saleProduct.getSale(), sale);
        System.out.println("testMethodSale method working");
    }
    @Test
    @DisplayName("product() method")
    void testMethodProduct() {
        final Product product= new Product(); product.setId(2L); product.setName("bonibon"); product.setUnit(Unit.PCS);
        final SaleProduct saleProduct= new SaleProduct();
        saleProduct.product(product);
        assertEquals(saleProduct.getProduct(), product);
        System.out.println("testMethodProduct method working");
    }
}