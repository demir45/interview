package tr.com.kolaysoft.manav.domain;

import org.junit.Assert;
import org.junit.jupiter.api.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class GroceryTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final Grocery grocery= new Grocery();
        grocery.setId(1L);
        assertEquals(grocery.getId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterName() {
        final String name="Erdal bakkal";
        final Grocery grocery= new Grocery();
        grocery.setName("Erdal bakkal");
        assertEquals(grocery.getName(), name);
        System.out.println("testGetterSetterName method working");
    }
    @Test
    @DisplayName("name() method")
    void testMethodName() {
        final String name="Erdal bakkal";
        final Grocery grocery= new Grocery();
        grocery.name("Erdal bakkal");
        assertEquals(grocery.getName(), name);
        System.out.println("testMethodName method working");
    }
    @Test
    void testGetterSetterCreatedDate() {
        final Instant instant=Instant.now();
        final Grocery grocery= new Grocery();
        assertEquals(grocery.getCreatedDate(), instant);
        System.out.println("testGetterSetterCreatedDate method working");
    }
    @Test
    void testGetterSetterlastModifiedDate() {
        final Instant instant=Instant.now();
        final Grocery grocery= new Grocery();
        assertEquals(grocery.getLastModifiedDate(), instant);
        System.out.println("testGetterSetterlastModifiedDate method working");
    }
}