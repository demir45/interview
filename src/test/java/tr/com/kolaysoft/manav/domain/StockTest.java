package tr.com.kolaysoft.manav.domain;

import org.junit.jupiter.api.*;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class StockTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final Stock stock= new Stock();
        stock.setId(1L);
        assertEquals(stock.getId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterCount() {
        final BigDecimal count= BigDecimal.valueOf(1345677888);
        final Stock stock= new Stock();
        stock.setCount(BigDecimal.valueOf(1345677888));
        assertEquals(stock.getCount(), count);
        System.out.println("testGetterSetterCount method working");
    }
    @Test
    void testGetterSetterGrocery() {
        final Stock stock= new Stock();
        Grocery grocery=new Grocery(); grocery.setId(1L); grocery.setName("Erdal bakkal");
        stock.setGrocery(grocery);
        assertEquals(stock.getGrocery(), grocery);
        System.out.println("testGetterSetterGrocery method working");
    }
    @Test
    void testGetterSetterProduct() {
        final Product product= new Product(); product.setId(5L);
        final Stock stock= new Stock();
        stock.setProduct(product);
        assertEquals(stock.getProduct(), product);
        System.out.println("testGetterSetterProduct method working");
    }

}