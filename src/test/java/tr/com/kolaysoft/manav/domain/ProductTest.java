package tr.com.kolaysoft.manav.domain;

import org.junit.jupiter.api.*;
import tr.com.kolaysoft.manav.domain.enumeration.Unit;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final Product product= new Product();
        product.setId(1L);
        assertEquals(product.getId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterName() {
        final String name="�okonat";
        final Product product= new Product();
        product.setName("�okonat");
        assertEquals(product.getName(), name);
        System.out.println("testGetterSetterName method working");
    }
    @Test
    void testGetterSetterUnit() {
        final Unit unit=Unit.KG;
        final Product product= new Product();
        product.setUnit(Unit.KG);
        assertEquals(product.getUnit(), unit);
        System.out.println("testGetterSetterUnit method working");
    }
    @Test
    @DisplayName("name() method")
    void testMethodName() {
        final String name="�okonat";
        final Product product= new Product();
        product.name("�okonat");
        assertEquals(product.getName(), name);
        System.out.println("testMethodName method working");
    }
}