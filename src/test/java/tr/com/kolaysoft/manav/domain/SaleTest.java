package tr.com.kolaysoft.manav.domain;

import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class SaleTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    @DisplayName("grocery() method")
    void testMethodGrocery() {
        final Sale sale= new Sale();
        Grocery grocery=new Grocery(); grocery.setId(1L); grocery.setName("Erdal bakkal");
        sale.grocery(grocery);
        assertEquals(sale.getGrocery(), grocery);
        System.out.println("testMethodGrocery method working");
    }
    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final Sale sale= new Sale();
        sale.setId(1L);
        assertEquals(sale.getId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterGrocery() {
        final Sale sale= new Sale();
        Grocery grocery=new Grocery(); grocery.setId(1L); grocery.setName("Erdal bakkal");
        sale.setGrocery(grocery);
        assertEquals(sale.getGrocery(), grocery);
        System.out.println("testGetterSetterGrocery method working");
    }
    @Test
    void testGetterSetterProduct() {
        final Sale sale= new Sale(); sale.setId(2L);
        final Product product= new Product(); product.setId(3L);
        final SaleProductId saleProductId= new SaleProductId();
        saleProductId.setSaleId(sale.getId());
        saleProductId.setProductId(product.getId());
        final SaleProduct saleProduct= new SaleProduct();
        saleProduct.setId(saleProductId);
        saleProduct.setCount(BigDecimal.valueOf(5));
        saleProduct.setPrice(BigDecimal.valueOf(6));
        final Set<SaleProduct> saleProducts=new HashSet<>();
        saleProducts.add(saleProduct);
        sale.setProducts(saleProducts);
        assertEquals(sale.getProducts(), saleProducts);
        System.out.println("testGetterSetterProduct method working");
    }
    @Test
    void testGetterSetterCreatedDate() {
        final Instant instant=Instant.now();
        final Sale sale= new Sale();
        assertEquals(sale.getCreatedDate(), instant);
        System.out.println("testGetterSetterCreatedDate method working");
    }
    @Test
    void testGetterSetterlastModifiedDate() {
        final Instant instant=Instant.now();
        final Sale sale= new Sale();
        assertEquals(sale.getLastModifiedDate(), instant);
        System.out.println("testGetterSetterlastModifiedDate method working");
    }
}