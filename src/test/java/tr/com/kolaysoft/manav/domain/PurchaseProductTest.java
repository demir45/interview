package tr.com.kolaysoft.manav.domain;

import org.junit.jupiter.api.*;
import tr.com.kolaysoft.manav.domain.enumeration.Unit;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class PurchaseProductTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    void testGetterSetterId() {
        final Purchase purchase= new Purchase(); purchase.setId(2L);
        final Product product= new Product(); product.setId(3L);
        final PurchaseProductId purchaseProductId= new PurchaseProductId();
        purchaseProductId.setPurchaseId(purchase.getId());
        purchaseProductId.setProductId(product.getId());
        final PurchaseProduct purchaseProduct= new PurchaseProduct();
        purchaseProduct.setId(purchaseProductId);
        assertEquals(purchaseProduct.getId().getProductId(),purchaseProductId.getProductId());
        assertEquals(purchaseProduct.getId().getPurchaseId(),purchaseProductId.getPurchaseId());
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterCount() {
        final BigDecimal count= BigDecimal.valueOf(1345677888);
        final PurchaseProduct purchaseProduct= new PurchaseProduct();
        purchaseProduct.setCount(BigDecimal.valueOf(1345677888));
        assertEquals(purchaseProduct.getCount(), count);
        System.out.println("testGetterSetterCount method working");
    }
    @Test
    void testGetterSetterPrice() {
        final BigDecimal price= BigDecimal.valueOf(1345677888);
        final PurchaseProduct purchaseProduct= new PurchaseProduct();
        purchaseProduct.setPrice(BigDecimal.valueOf(1345677888));
        assertEquals(purchaseProduct.getPrice(), price);
        System.out.println("testGetterSetterPrice method working");
    }
    @Test
    void testGetterSetterPurchase() {
        final Purchase purchase= new Purchase(); purchase.setId(5L);
        final PurchaseProduct purchaseProduct= new PurchaseProduct();
        purchaseProduct.setPurchase(purchase);
        assertEquals(purchaseProduct.getPurchase(), purchase);
        System.out.println("testGetterSetterPurchase method working");
    }
    @Test
    void testGetterSetterProduct() {
        final Product product= new Product(); product.setId(5L);
        final PurchaseProduct purchaseProduct= new PurchaseProduct();
        purchaseProduct.setProduct(product);
        assertEquals(purchaseProduct.getProduct(), product);
        System.out.println("testGetterSetterProduct method working");
    }
    @Test
    @DisplayName("count() method")
    void testMethodCount() {
        final BigDecimal count= BigDecimal.valueOf(1345677888);
        final PurchaseProduct purchaseProduct= new PurchaseProduct();
        purchaseProduct.count(BigDecimal.valueOf(1345677888));
        assertEquals(purchaseProduct.getCount(), count);
        System.out.println("testMethodCount method working");
    }
    @Test
    @DisplayName("price() method")
    void testMethodPrice() {
        final BigDecimal price= BigDecimal.valueOf(1345677888);
        final PurchaseProduct purchaseProduct= new PurchaseProduct();
        purchaseProduct.price(BigDecimal.valueOf(1345677888));
        assertEquals(purchaseProduct.getPrice(), price);
        System.out.println("testMethodPrice method working");
    }
    @Test
    @DisplayName("purchase() method")
    void testMethodPurchase() {
        final Grocery grocery=new Grocery(); grocery.setName("Erdal bakkal");
        final Purchase purchase= new Purchase(); purchase.setId(2L); purchase.setGrocery(grocery);
        final PurchaseProduct purchaseProduct= new PurchaseProduct();
        purchaseProduct.purchase(purchase);
        assertEquals(purchaseProduct.getPurchase(), purchase);
        System.out.println("testMethodPurchase method working");
    }
    @Test
    @DisplayName("product() method")
    void testMethodProduct() {
        final Product product= new Product(); product.setId(2L); product.setName("bonibon"); product.setUnit(Unit.PCS);
        final PurchaseProduct purchaseProduct= new PurchaseProduct();
        purchaseProduct.product(product);
        assertEquals(purchaseProduct.getProduct(), product);
        System.out.println("testMethodProduct method working");
    }
}