package tr.com.kolaysoft.manav.domain;

import org.junit.jupiter.api.*;
import tr.com.kolaysoft.manav.domain.enumeration.Unit;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PurchaseTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    @DisplayName("grocery() method")
    void testMethodGrocery() {
        final Purchase purchase= new Purchase();
        Grocery grocery=new Grocery(); grocery.setId(1L); grocery.setName("Erdal bakkal");
        purchase.grocery(grocery);
        assertEquals(purchase.getGrocery(), grocery);
        System.out.println("testMethodGrocery method working");
    }
    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final Purchase purchase= new Purchase();
        purchase.setId(1L);
        assertEquals(purchase.getId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterGrocery() {
        final Purchase purchase= new Purchase();
        Grocery grocery=new Grocery(); grocery.setId(1L); grocery.setName("Erdal bakkal");
        purchase.setGrocery(grocery);
        assertEquals(purchase.getGrocery(), grocery);
        System.out.println("testGetterSetterGrocery method working");
    }
    @Test
    void testGetterSetterCreatedDate() {
        final Instant instant=Instant.now();
        final Purchase purchase= new Purchase();
        assertEquals(purchase.getCreatedDate(), instant);
        System.out.println("testGetterSetterCreatedDate method working");
    }
    @Test
    void testGetterSetterlastModifiedDate() {
        final Instant instant=Instant.now();
        final Purchase purchase= new Purchase();
        assertEquals(purchase.getLastModifiedDate(), instant);
        System.out.println("testGetterSetterlastModifiedDate method working");
    }
}