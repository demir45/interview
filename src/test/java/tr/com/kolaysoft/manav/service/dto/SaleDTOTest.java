package tr.com.kolaysoft.manav.service.dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class SaleDTOTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final SaleDTO saleDTO= new SaleDTO();
        saleDTO.setId(1L);
        assertEquals(saleDTO.getId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterGrocery() {
        final SaleDTO saleDTO= new SaleDTO();
        final GroceryDTO groceryDTO=new GroceryDTO(); groceryDTO.setId(1L); groceryDTO.setName("Erdal bakkal");
        saleDTO.setGrocery(groceryDTO);
        assertEquals(saleDTO.getGrocery(), groceryDTO);
        System.out.println("testGetterSetterGrocery method working");
    }
    @Test
    void testGetterSetterCreatedDate() {
        final Instant instant=Instant.now();
        final SaleDTO saleDTO= new SaleDTO();
        assertEquals(saleDTO.getCreatedDate(), instant);
        System.out.println("testGetterSetterCreatedDate method working");
    }
    @Test
    void testGetterSetterLastModifiedDate() {
        final Instant instant=Instant.now();
        final SaleDTO saleDTO= new SaleDTO();
        assertEquals(saleDTO.getLastModifiedDate(), instant);
        System.out.println("testGetterSetterLastModifiedDate method working");
    }

}