package tr.com.kolaysoft.manav.service.dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import tr.com.kolaysoft.manav.domain.Grocery;
import tr.com.kolaysoft.manav.domain.enumeration.Unit;

import static org.junit.jupiter.api.Assertions.*;

class ProductDTOTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final ProductDTO productDTO= new ProductDTO();
        productDTO.setId(1L);
        assertEquals(productDTO.getId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterName() {
        final String name="bonibon";
        final ProductDTO productDTO= new ProductDTO();
        productDTO.setName("bonibon");
        assertEquals(productDTO.getName(), name);
        System.out.println("testGetterSetterName method working");
    }
    @Test
    void testGetterSetterUnit() {
        final Unit name=Unit.KG;
        final ProductDTO productDTO= new ProductDTO();
        productDTO.setUnit(Unit.KG);
        assertEquals(productDTO.getUnit(), name);
        System.out.println("testGetterSetterUnit method working");
    }
}