package tr.com.kolaysoft.manav.service.dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import tr.com.kolaysoft.manav.domain.Grocery;
import tr.com.kolaysoft.manav.domain.Sale;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class PurchaseDTOTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final PurchaseDTO purchaseDTO= new PurchaseDTO();
        purchaseDTO.setId(1L);
        assertEquals(purchaseDTO.getId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterGrocery() {
        final PurchaseDTO purchaseDTO= new PurchaseDTO();
        final GroceryDTO groceryDTO=new GroceryDTO(); groceryDTO.setId(1L); groceryDTO.setName("Erdal bakkal");
        purchaseDTO.setGrocery(groceryDTO);
        assertEquals(purchaseDTO.getGrocery(), groceryDTO);
        System.out.println("testGetterSetterGrocery method working");
    }
    @Test
    void testGetterSetterCreatedDate() {
        final Instant instant=Instant.now();
        final PurchaseDTO purchaseDTO= new PurchaseDTO();
        assertEquals(purchaseDTO.getCreatedDate(), instant);
        System.out.println("testGetterSetterCreatedDate method working");
    }
    @Test
    void testGetterSetterLastModifiedDate() {
        final Instant instant=Instant.now();
        final PurchaseDTO purchaseDTO= new PurchaseDTO();
        assertEquals(purchaseDTO.getLastModifiedDate(), instant);
        System.out.println("testGetterSetterLastModifiedDate method working");
    }

}