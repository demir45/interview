package tr.com.kolaysoft.manav.service.dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import tr.com.kolaysoft.manav.domain.Grocery;
import tr.com.kolaysoft.manav.domain.SaleProduct;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class ProductSalePurchaseDTOTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final ProductSalePurchaseDTO productSalePurchaseDTO= new ProductSalePurchaseDTO();
        productSalePurchaseDTO.setProductId(1L);
        assertEquals(productSalePurchaseDTO.getProductId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterCount() {
        final BigDecimal count= BigDecimal.valueOf(1345677888);
        final ProductSalePurchaseDTO productSalePurchaseDTO= new ProductSalePurchaseDTO();
        productSalePurchaseDTO.setCount(BigDecimal.valueOf(1345677888));
        assertEquals(productSalePurchaseDTO.getCount(), count);
        System.out.println("testGetterSetterCount method working");
    }
    @Test
    void testGetterSetterPrice() {
        final BigDecimal price= BigDecimal.valueOf(1345677888);
        final ProductSalePurchaseDTO productSalePurchaseDTO= new ProductSalePurchaseDTO();
        productSalePurchaseDTO.setPrice(BigDecimal.valueOf(1345677888));
        assertEquals(productSalePurchaseDTO.getPrice(), price);
        System.out.println("testGetterSetterPrice method working");
    }

}