package tr.com.kolaysoft.manav.service.dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import tr.com.kolaysoft.manav.domain.Grocery;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class GroceryDTOTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final GroceryDTO groceryDTO= new GroceryDTO();
        groceryDTO.setId(1L);
        assertEquals(groceryDTO.getId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterName() {
        final String name="Erdal bakkal";
        final GroceryDTO groceryDTO= new GroceryDTO();
        groceryDTO.setName("Erdal bakkal");
        assertEquals(groceryDTO.getName(), name);
        System.out.println("testGetterSetterName method working");
    }
    @Test
    void testGetterSetterCreatedDate() {
        final Instant instant=Instant.now();
        final GroceryDTO groceryDTO= new GroceryDTO();
        assertEquals(groceryDTO.getCreatedDate(), instant);
        System.out.println("testGetterSetterCreatedDate method working");
    }
    @Test
    void testGetterSetterLastModifiedDate() {
        final Instant instant=Instant.now();
        final GroceryDTO groceryDTO= new GroceryDTO();
        assertEquals(groceryDTO.getLastModifiedDate(), instant);
        System.out.println("testGetterSetterLastModifiedDate method working");
    }

}