package tr.com.kolaysoft.manav.service.dto;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import tr.com.kolaysoft.manav.domain.Grocery;
import tr.com.kolaysoft.manav.domain.Product;
import tr.com.kolaysoft.manav.domain.Stock;

import java.math.BigDecimal;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class StockDTOTest {

    @BeforeEach
    void setUp(TestInfo info) {

        System.out.println(info.getDisplayName()+" worked");
    }

    @AfterEach
    void tearDown(TestInfo info) {
        System.out.println(info.getDisplayName()+" test finished");
    }

    @Test
    void testGetterSetterId() {
        final Long id=1L;
        final StockDTO stockDTO= new StockDTO();
        stockDTO.setId(1L);
        assertEquals(stockDTO.getId(), id);
        System.out.println("testGetterSetterId method working");
    }
    @Test
    void testGetterSetterGrocery() {
        final StockDTO stockDTO= new StockDTO();
        final GroceryDTO groceryDTO=new GroceryDTO(); groceryDTO.setId(1L); groceryDTO.setName("Erdal bakkal");
        stockDTO.setGrocery(groceryDTO);
        assertEquals(stockDTO.getGrocery(), groceryDTO);
        System.out.println("testGetterSetterGrocery method working");
    }
    @Test
    void testGetterSetterProduct() {
        final Product product= new Product(); product.setId(5L);
        final StockDTO stockDTO= new StockDTO();
        stockDTO.setProduct(product);
        assertEquals(stockDTO.getProduct(), product);
        System.out.println("testGetterSetterProduct method working");
    }
    @Test
    void testGetterSetterCount() {
        final BigDecimal count= BigDecimal.valueOf(1345677888);
        final StockDTO stockDTO= new StockDTO();
        stockDTO.setCount(BigDecimal.valueOf(1345677888));
        assertEquals(stockDTO.getCount(), count);
        System.out.println("testGetterSetterCount method working");
    }
    @Test
    void testGetterSetterCreatedDate() {
        final Instant instant=Instant.now();
        final StockDTO stockDTO= new StockDTO();
        assertEquals(stockDTO.getCreatedDate(), instant);
        System.out.println("testGetterSetterCreatedDate method working");
    }
    @Test
    void testGetterSetterLastModifiedDate() {
        final Instant instant=Instant.now();
        final StockDTO stockDTO= new StockDTO();
        assertEquals(stockDTO.getLastModifiedDate(), instant);
        System.out.println("testGetterSetterLastModifiedDate method working");
    }
}