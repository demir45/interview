package tr.com.kolaysoft.manav.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A Sale.
 */
@Entity
@Table(name = "stock")
public class Stock extends AbstractAuditingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "count", precision = 21, scale = 2, nullable = false)
    private BigDecimal count;

    @ManyToOne(optional = false)
    @NotNull
    private Grocery grocery;

    @ManyToOne(optional = false)
    //@MapsId("productId")
    @NotNull
    Product product;

    public Stock() {
    }

    public Stock(Long id, BigDecimal count, Grocery grocery,Product product) {
        this.id = id;
        this.count = count;
        this.grocery = grocery;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCount() {
        return count;
    }

    public void setCount(BigDecimal count) {
        this.count = count;
    }

    public Grocery getGrocery() {
        return grocery;
    }

    public void setGrocery(Grocery grocery) {
        this.grocery = grocery;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Stock)) return false;
        Stock stock = (Stock) o;
        return Objects.equals(id, stock.id) && Objects.equals(count, stock.count) && Objects.equals(grocery, stock.grocery) && Objects.equals(product, stock.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, count, grocery, product);
    }

    @Override
    public String toString() {
        return "Stock{" +
                "id=" + getId() +
                ", count=" + getCount() +
                ", grocery=" + getGrocery() +
                ", product=" + getProduct() +
                '}';
    }
}
