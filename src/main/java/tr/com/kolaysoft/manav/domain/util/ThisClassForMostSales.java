package tr.com.kolaysoft.manav.domain.util;

// This class created for storing information
public class ThisClassForMostSales {

    private String productId;

    private String urunAdi;

    private String totalCount;

    public ThisClassForMostSales() {
    }

    public ThisClassForMostSales(String productId, String urunAdi, String totalCount) {
        this.urunAdi = urunAdi;
        this.productId = productId;
        this.totalCount = totalCount;
    }

    public String getUrunAdi() {
        return urunAdi;
    }

    public void setUrunAdi(String urunAdi) {
        this.urunAdi = urunAdi;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public String toString() {
        return "ThisClassForMostSales{" +
                "urunAdi='" + urunAdi + '\'' +
                ", productId='" + productId + '\'' +
                ", totalCount='" + totalCount + '\'' +
                '}';
    }
}
