package tr.com.kolaysoft.manav.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tr.com.kolaysoft.manav.repository.StockRepository;
import tr.com.kolaysoft.manav.service.StockService;
import tr.com.kolaysoft.manav.service.dto.StockDTO;
import tr.com.kolaysoft.manav.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
/**
 * REST controller for managing {@link tr.com.kolaysoft.manav.domain.Stock}.
 */
@RestController
@RequestMapping("/api")
public class StockResource {

    private final static Logger LOGGER = LoggerFactory.getLogger(StockResource.class);

    private final StockRepository stockRepository;

    private final StockService stockService;

    public StockResource(StockRepository stockRepository, StockService stockService) {
        this.stockRepository = stockRepository;
        this.stockService = stockService;
    }
/*
    !! while puchase and sale  creating, also stock creating
     * {@code POST  /stock} : Create a new stock.
     *
     * @param stockDTO the stockDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new stockDTO, or with status {@code 400 (Bad Request)} if the stock has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.

    @PostMapping("/stock")
    public ResponseEntity<StockDTO> createStock(@Valid @RequestBody StockDTO stockDTO) throws URISyntaxException {
        if (stockDTO.getId() != null) {
            throw new BadRequestAlertException("A new product cannot already have an ID");
        }
        return new ResponseEntity<>(stockService.save(stockDTO), HttpStatus.CREATED);
    }

 */
    /**
     * {@code PUT  /stock} : Updates an existing stock.
     *
     * @param stockDTO the stockDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated stockDTO,
     * or with status {@code 400 (Bad Request)} if the stockDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the stockDTO couldn't be updated.
     */
    @PutMapping("/stock")
    public ResponseEntity<StockDTO> updateStock(@Valid @RequestBody StockDTO stockDTO) {

        if (stockDTO.getId() == null) {
            LOGGER.error("Invalid id");
            throw new BadRequestAlertException("Invalid id");
        }

        if (!stockRepository.existsById(stockDTO.getId())) {
            LOGGER.error("Entity not found");
            throw new BadRequestAlertException("Entity not found");
        }
        LOGGER.info("updateStock method triggered");
        return new ResponseEntity<>(stockService.save(stockDTO), HttpStatus.OK);
    }
    /**
     * {@code GET  /stock} : get all the stock.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of stock in body.
     */
    @GetMapping("/stock")
    public ResponseEntity<List<StockDTO>> getAllStock() {
        LOGGER.info("getAllStock method triggered");
        return new ResponseEntity<>(stockService.findAll(), HttpStatus.OK);
    }
    /**
     * {@code GET  /stock/:id} : get the "id" stock.
     *
     * @param id the id of the stockDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the stockDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/stock/{id}")
    public ResponseEntity<StockDTO> getstock(@PathVariable Long id) {
        LOGGER.info("getstock method triggered");
        return new ResponseEntity(stockService.findOne(id).orElse(null),HttpStatus.OK);
    }
    /**
     * {@code DELETE  /stock/:id} : delete the "id" stock.
     *
     * @param id the id of the stockDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/stock/{id}")
    public ResponseEntity<Map<String, Boolean>> deleteStock(@PathVariable Long id) {
        LOGGER.info("deleteStock method triggered");
        return new ResponseEntity<>(stockService.delete(id), HttpStatus.OK);

    }
}
