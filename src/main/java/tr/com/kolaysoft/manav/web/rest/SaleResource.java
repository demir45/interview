package tr.com.kolaysoft.manav.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tr.com.kolaysoft.manav.domain.util.ThisClassForMostSales;
import tr.com.kolaysoft.manav.repository.SaleRepository;
import tr.com.kolaysoft.manav.service.SaleService;
import tr.com.kolaysoft.manav.service.dto.SaleDTO;
import tr.com.kolaysoft.manav.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link tr.com.kolaysoft.manav.domain.Sale}.
 */
@RestController
@RequestMapping("/api")
public class SaleResource {

    private final static Logger LOGGER = LoggerFactory.getLogger(SaleResource.class);

    private final SaleService saleService;

    private final SaleRepository saleRepository;

    public SaleResource(SaleService saleService, SaleRepository saleRepository) {
        this.saleService = saleService;
        this.saleRepository = saleRepository;
    }

    /**
     * {@code POST  /sales} : Create a new sale.
     *
     * @param saleDTO the saleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new saleDTO, or with status {@code 400 (Bad Request)} if the sale has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sales")
    public ResponseEntity<SaleDTO> createSale(@Valid @RequestBody SaleDTO saleDTO) throws URISyntaxException {
        if (saleDTO.getId() != null) {
            LOGGER.error("A new sale cannot already have an ID");
            throw new BadRequestAlertException("A new sale cannot already have an ID");
        }
        SaleDTO result = saleService.save(saleDTO);
        LOGGER.info("createSale method triggered");
        return ResponseEntity.created(new URI("/api/sales/" + result.getId())).body(result);
    }

    /**
     * {@code PUT  /sales} : Updates an existing sale.
     *
     * @param saleDTO the saleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated saleDTO,
     * or with status {@code 400 (Bad Request)} if the saleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the saleDTO couldn't be updated.
     */
    @PutMapping("/sales")
    public ResponseEntity<SaleDTO> updateSale(@Valid @RequestBody SaleDTO saleDTO) {
        if (saleDTO.getId() == null) {
            LOGGER.error("Invalid id");
            throw new BadRequestAlertException("Invalid id");
        }

        if (!saleRepository.existsById(saleDTO.getId())) {
            LOGGER.error("Entity not found");
            throw new BadRequestAlertException("Entity not found");
        }

        SaleDTO result = saleService.save(saleDTO);
        LOGGER.info("updateSale method triggered");
        return ResponseEntity.ok().body(result);
    }

    /**
     * {@code GET  /sales} : get all the sales.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sales in body.
     */
    @GetMapping("/sales")
    public List<SaleDTO> getAllSales() {
        LOGGER.info("getAllSales method triggered");
        return saleService.findAll();
    }

    /**
     * {@code GET  /sales/:id} : get the "id" sale.
     *
     * @param id the id of the saleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the saleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sales/{id}")
    public ResponseEntity<SaleDTO> getSale(@PathVariable Long id) {
        Optional<SaleDTO> saleDTO = saleService.findOne(id);
        LOGGER.info("getSale method triggered");
        return ResponseEntity.ok(saleDTO.orElse(null));
    }

    /**
     * {@code DELETE  /sales/:id} : delete the "id" sale.
     *
     * @param id the id of the saleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sales/{id}")
    public ResponseEntity<Void> deleteSale(@PathVariable Long id) {
        saleService.delete(id);
        LOGGER.info("deleteSale method triggered");
        return ResponseEntity.noContent().build();
    }
    /**
     * {@code POST  /sales/{groceryId}/{month}} : Create a new sale.
     *
     * @param groceryId find grocer stock.
     * @param month find this month stock.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new saleDTO, or with status {@code 400 (Bad Request)} if the sale has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sales/{groceryId}/{month}")
    public ResponseEntity<List<ThisClassForMostSales>> getMostSoldStock(@PathVariable Long groceryId,
                                                                        @PathVariable int month) {

        return new ResponseEntity<>(saleService.getMostSoldStock(groceryId, month), HttpStatus.OK);
    }
    

}
