package tr.com.kolaysoft.manav.web.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tr.com.kolaysoft.manav.repository.PurchaseRepository;
import tr.com.kolaysoft.manav.service.PurchaseService;
import tr.com.kolaysoft.manav.service.dto.PurchaseDTO;
import tr.com.kolaysoft.manav.web.rest.errors.BadRequestAlertException;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
/**
 * REST controller for managing {@link tr.com.kolaysoft.manav.domain.Purchase}.
 */
@RestController
@RequestMapping("/api")
public class PurchaseResource {

    private final static Logger LOGGER = LoggerFactory.getLogger(PurchaseResource.class);

    private final PurchaseService purchaseService;

    private final PurchaseRepository purchaseRepository;

    public PurchaseResource(PurchaseService purchaseService, PurchaseRepository purchaseRepository) {
        this.purchaseService = purchaseService;
        this.purchaseRepository = purchaseRepository;
    }
    /**
     * {@code POST  /purchase} : Create a new purchase.
     *
     * @param purchaseDTO the purchaseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new purchaseDTO, or with status {@code 400 (Bad Request)} if the purchase has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/purchase")
    public ResponseEntity<PurchaseDTO> createPurchase(@Valid @RequestBody PurchaseDTO purchaseDTO) throws URISyntaxException {
        if (purchaseDTO.getId() != null) {
            LOGGER.error("A new purchase cannot already have an ID");
            throw new BadRequestAlertException("A new purchase cannot already have an ID");
        }
        LOGGER.info("createPurchase method triggered");
        return new ResponseEntity<>(purchaseService.save(purchaseDTO), HttpStatus.CREATED);
    }
    /**
     * {@code PUT  /purchase} : Updates an existing purchase.
     *
     * @param purchaseDTO the purchaseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated purchaseDTO,
     * or with status {@code 400 (Bad Request)} if the purchaseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the purchaseDTO couldn't be updated.
     */
    @PutMapping("/purchase")
    public ResponseEntity<PurchaseDTO> updatePurchase(@Valid @RequestBody PurchaseDTO purchaseDTO) {

        if (purchaseDTO.getId() == null) {
            LOGGER.error("Invalid id");
            throw new BadRequestAlertException("Invalid id");
        }

        if (!purchaseRepository.existsById(purchaseDTO.getId())) {
            LOGGER.error("Entity not found");
            throw new BadRequestAlertException("Entity not found");
        }
        LOGGER.info("updatePurchase method triggered");
        return new ResponseEntity<>(purchaseService.save(purchaseDTO), HttpStatus.OK);
    }
    /**
     * {@code GET  /purchase} : get all the purchase.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of purchase in body.
     */
    @GetMapping("/purchase")
    public ResponseEntity<List<PurchaseDTO>> getAllPurchases() {
        LOGGER.info("getAllPurchases method triggered");
        return new ResponseEntity<>(purchaseService.findAll(), HttpStatus.OK);
    }

    /**
     * {@code GET  /purchase/:id} : get the "id" purchase.
     *
     * @param id the id of the purchaseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the purchaseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/purchase/{id}")
    public ResponseEntity<PurchaseDTO> getPurchase(@PathVariable Long id) {
        LOGGER.info("getPurchase method triggered");
        return new ResponseEntity(purchaseService.findOne(id).orElse(null),HttpStatus.OK);
    }
    /**
     * {@code DELETE  /purchase/:id} : delete the "id" purchase.
     *
     * @param id the id of the purchaseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/purchase/{id}")
    public ResponseEntity<Map<String, Boolean>> deletePurchase(@PathVariable Long id) {
        LOGGER.info("deletePurchase method triggered");
        return new ResponseEntity<>(purchaseService.delete(id), HttpStatus.OK);

    }
}
