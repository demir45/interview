package tr.com.kolaysoft.manav.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.kolaysoft.manav.domain.Stock;
import tr.com.kolaysoft.manav.repository.StockRepository;
import tr.com.kolaysoft.manav.service.dto.StockDTO;
import tr.com.kolaysoft.manav.service.mapper.StockMapper;

import java.util.*;
import java.util.stream.Collectors;
/**
 * Service Implementation for managing {@link Stock}.
 */
@Service
@Transactional
public class StockService {

    private final static Logger LOGGER = LoggerFactory.getLogger(StockService.class);

    private final StockRepository stockRepository;

    private final StockMapper stockMapper;

    public StockService(StockRepository stockRepository, StockMapper stockMapper) {
        this.stockRepository = stockRepository;
        this.stockMapper = stockMapper;
    }
    /**
     * Save a stock.
     *
     * @param stockDTO the entity to save.
     * @return the persisted entity.
     */
    public StockDTO save(StockDTO stockDTO) {
        Stock stock = stockMapper.toEntity(stockDTO);

        stock = stockRepository.save(stock);
        LOGGER.info("save method triggered");
        return stockMapper.toDto(stock);
    }

    /**
     * Get all the stock.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<StockDTO> findAll() {
        LOGGER.info("findAll method triggered");
        return stockRepository.findAll()
                .stream()
                .map(stockMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }
    /**
     * Get one stock by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<StockDTO> findOne(Long id) {
        LOGGER.info("findOne method triggered");
        return stockRepository.findById(id).map(stockMapper::toDto);
    }
    /**
     * Delete the stock by id.
     *
     * @param id the id of the entity.
     */
    public Map<String, Boolean> delete(Long id) {

        Map<String, Boolean> map = new HashMap<>();
        if(stockRepository.existsById(id)){
            stockRepository.deleteById(id);
            map.put("Stock deleted successfully", true);
        }else{
            map.put("id not found", false);
            LOGGER.error("id not found");
        }
        LOGGER.info("delete method triggered");
        return map;
    }

}
