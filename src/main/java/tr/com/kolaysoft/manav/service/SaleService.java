package tr.com.kolaysoft.manav.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.kolaysoft.manav.domain.Product;
import tr.com.kolaysoft.manav.domain.Sale;
import tr.com.kolaysoft.manav.domain.SaleProduct;
import tr.com.kolaysoft.manav.domain.Stock;
import tr.com.kolaysoft.manav.domain.util.ThisClassForMostSales;
import tr.com.kolaysoft.manav.repository.ProductRepository;
import tr.com.kolaysoft.manav.repository.SaleProductRepository;
import tr.com.kolaysoft.manav.repository.SaleRepository;
import tr.com.kolaysoft.manav.repository.StockRepository;
import tr.com.kolaysoft.manav.service.dto.SaleDTO;
import tr.com.kolaysoft.manav.service.mapper.SaleMapper;
import tr.com.kolaysoft.manav.web.rest.errors.InsufficientAmountException;
import tr.com.kolaysoft.manav.web.rest.errors.ResourceNotFoundException;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Sale}.
 */
@Service
@Transactional
public class SaleService {

    private final static Logger LOGGER = LoggerFactory.getLogger(SaleService.class);

    private final static String COUNT_NOT_NEGATIVE_MSG = "count %d can not be negative";

    private final static String PRODUCT_COUNT_NOT_BIGGER_MSG = "product count %d must be less than stock count";

    private final static String PRODUCT_NOT_FOUND_MSG = "product %s not found";

    private final SaleRepository saleRepository;

    private final SaleMapper saleMapper;

    private final ProductRepository productRepository;

    public final StockRepository stockRepository;

    public final SaleProductRepository saleProductRepository;


    public SaleService(SaleRepository saleRepository, SaleMapper saleMapper, ProductRepository productRepository, StockRepository stockRepository, SaleProductRepository saleProductRepository) {
        this.saleRepository = saleRepository;
        this.saleMapper = saleMapper;
        this.productRepository = productRepository;
        this.stockRepository = stockRepository;
        this.saleProductRepository = saleProductRepository;
    }

    /**
     * Save a sale.
     *
     * @param saleDTO the entity to save.
     * @return the persisted entity.
     */
    public SaleDTO save(SaleDTO saleDTO) {

        Sale sale = saleMapper.toEntity(saleDTO);

        if (saleDTO.getId() == null) {

            Set<Stock> stocks = stockRepository.findByGroceryId(sale.getGrocery().getId());

            final Set<SaleProduct> products = saleDTO
                    .getProducts()
                    .stream()
                    .map(product -> new SaleProduct()
                            .product(productRepository.findById(product.getProductId()).orElse(null))
                            .count(product.getCount())
                            .price(product.getPrice())
                    )
                    .collect(Collectors.toSet());
            sale.setProducts(products);
            // control for product
            for (SaleProduct product : products) {
                if (stocks.stream().
                        noneMatch(stock -> stock.getProduct().getId() == product.getProduct().getId())) {
                    LOGGER.error(String.format(PRODUCT_NOT_FOUND_MSG, product.getProduct().getName()));
                    throw new ResourceNotFoundException(String.format(PRODUCT_NOT_FOUND_MSG, product.getProduct().getName()));
                }
            }

            Sale finalSale = sale;

            products.stream().forEach(product -> {
                        int value = product.getCount().intValue();
                        if (value < 0) {
                            LOGGER.error(String.format(COUNT_NOT_NEGATIVE_MSG, value));
                            throw new InsufficientAmountException(String.format(COUNT_NOT_NEGATIVE_MSG, value));
                        }
                        int control[] = {0};
                        for (Stock stock : stocks) {
                            if (stock.getProduct().getId() == product.getProduct().getId()) {
                                int stockValue = stock.getCount().intValue();
                                if (stockValue < value) {
                                    LOGGER.error(String.format(PRODUCT_COUNT_NOT_BIGGER_MSG, value));
                                    throw new InsufficientAmountException(String.format(PRODUCT_COUNT_NOT_BIGGER_MSG, value));
                                }
                                stock.setCount(stock.getCount().subtract(product.getCount()));
                                control[0] = 1;
                            }
                        }
                    }
            );

            sale = saleRepository.save(sale);
            for (SaleProduct saleProduct : products) {
                saleProductRepository.save(saleProduct);
            }
        }
        LOGGER.info("save method triggered");
        return saleMapper.toDto(sale);
    }

    /**
     * Get all the sales.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<SaleDTO> findAll() {
        LOGGER.info("findAll method triggered");
        return saleRepository.findAll()
                .stream()
                .map(saleMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one sale by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SaleDTO> findOne(Long id) {
        LOGGER.info("findOne method triggered");
        return saleRepository.findById(id).map(saleMapper::toDto);
    }

    /**
     * Delete the sale by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        LOGGER.info("delete method triggered");
        saleProductRepository.deleteBySaleId(id);
        saleRepository.deleteById(id);
    }
    /**
     * Get most sold 3 stock the sale by id.
     *
     * @param groceryId the id of the entity.
     * @param month .
     */
    public List<ThisClassForMostSales> getMostSoldStock(Long groceryId, int month) {
        //
        List<ThisClassForMostSales> thisClassForMostSalesList = new ArrayList<>();

        Map<Long, BigDecimal> productIdAndCountMap = new HashMap<>();
        List<Product> products = productRepository.findAll();
        for (Product product : products) {
            productIdAndCountMap.put(product.getId(), BigDecimal.valueOf(0));
        }

        List<Sale> allSales = saleRepository.findByGroceryId(groceryId);
        List<Sale> sales = new ArrayList<>();

        for (Sale sale : allSales) {
            String controlMonth = sale.getCreatedDate().toString().substring(5, 7);

            if (Integer.parseInt(controlMonth) == month) {
                sales.add(sale);
            }
        }
        List<SaleProduct> saleProducts = new ArrayList<>();
        Set<Long> productIdSet = new HashSet<>();
        for (Sale sale : sales) {
            List<SaleProduct> addSaleProducts = saleProductRepository.findBySaleId(sale.getId());
            addSaleProducts.stream().forEach(t -> saleProducts.add(t));
        }

        for (SaleProduct saleProduct : saleProducts) {
            // count add
            BigDecimal totalCount = productIdAndCountMap.get(saleProduct.getId().getProductId()).add(saleProduct.getCount());
            productIdAndCountMap.put(saleProduct.getProduct().getId(), totalCount);

        }
        Set<Long> mapKeys = productIdAndCountMap.keySet();
        Long removeItem = 0L;
        String urunAdi = "";
        String urunId = "";
        String urunMiktari = "";
        for (int r = 1; r <= 3; r++) {
            Long mapKeysArr[] = new Long[mapKeys.size()];
            int z = 0;
            for (Long mapKey : mapKeys) {

                mapKeysArr[z] = mapKey;
                z++;
            }
            // Outer loop
            for (int i = 0; i < productIdAndCountMap.size(); i++) {

                // Inner nested loop pointing 1 index ahead
                for (int j = i + 1; j < productIdAndCountMap.size(); j++) {

                    // Checking elements
                    Long temp = 0L;
                    BigDecimal first = productIdAndCountMap.get(mapKeysArr[i]);
                    BigDecimal second = productIdAndCountMap.get(mapKeysArr[j]);

                    if (first.compareTo(second) > 0) {

                        // Swapping
                        temp = mapKeysArr[i];

                        mapKeysArr[i] = mapKeysArr[j];
                        mapKeysArr[j] = temp;
                    }
                }
                for (Product product : products) {
                    if (product.getId() == mapKeysArr[i]) {
                        urunAdi = product.getName();
                        urunId = product.getId().toString();
                        urunMiktari = productIdAndCountMap.get(mapKeysArr[i]).toString();

                    }

                }
                removeItem = mapKeysArr[i];
            }
            thisClassForMostSalesList.add(new ThisClassForMostSales(urunId, urunAdi, urunMiktari));
            productIdAndCountMap.remove(removeItem);

        }

        return thisClassForMostSalesList;
    }


}