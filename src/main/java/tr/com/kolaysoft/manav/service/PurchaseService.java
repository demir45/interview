package tr.com.kolaysoft.manav.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.kolaysoft.manav.domain.Purchase;
import tr.com.kolaysoft.manav.domain.PurchaseProduct;
import tr.com.kolaysoft.manav.domain.Stock;
import tr.com.kolaysoft.manav.repository.ProductRepository;
import tr.com.kolaysoft.manav.repository.PurchaseProductRepository;
import tr.com.kolaysoft.manav.repository.PurchaseRepository;
import tr.com.kolaysoft.manav.repository.StockRepository;
import tr.com.kolaysoft.manav.service.dto.PurchaseDTO;
import tr.com.kolaysoft.manav.service.mapper.PurchaseMapper;

import java.util.*;
import java.util.stream.Collectors;
/**
 * Service Implementation for managing {@link Purchase}.
 */
@Service
@Transactional
public class PurchaseService {

    private final static Logger LOGGER = LoggerFactory.getLogger(PurchaseService.class);

    private final PurchaseRepository purchaseRepository;

    private final PurchaseMapper purchaseMapper;

    private final ProductRepository productRepository;

    private final StockRepository stockRepository;

    private final PurchaseProductRepository purchaseProductRepository;

    private final StockService stockService;

    public PurchaseService(PurchaseRepository purchaseRepository,
                           PurchaseMapper purchaseMapper,
                           ProductRepository productRepository, StockRepository stockRepository, PurchaseProductRepository purchaseProductRepository, StockService stockService) {
        this.purchaseRepository = purchaseRepository;
        this.purchaseMapper = purchaseMapper;
        this.productRepository = productRepository;
        this.stockRepository = stockRepository;
        this.purchaseProductRepository = purchaseProductRepository;
        this.stockService = stockService;
    }
    /**
     * Save a purchase.
     *
     * @param purchaseDTO the entity to save.
     * @return the persisted entity.
     */
    public PurchaseDTO save(PurchaseDTO purchaseDTO) {

        Purchase purchase = purchaseMapper.toEntity(purchaseDTO);

        if (purchaseDTO.getId() == null) {

            Set<Stock> stocks = stockRepository.findByGroceryId(purchase.getGrocery().getId());

            final Set<PurchaseProduct> products = purchaseDTO
                    .getProducts()
                    .stream()
                    .map(product -> new PurchaseProduct()
                            .product(productRepository.findById(product.getProductId()).orElse(null))
                            .count(product.getCount())
                            .price(product.getPrice())
                    )
                    .collect(Collectors.toSet());
            purchase.setProducts(products);
            purchase = purchaseRepository.save(purchase);

            Purchase finalPurchase = purchase;
            // if stock doesn't have data create new data
            if(stocks.isEmpty()){
                products.stream().forEach(product -> {
                    Stock stockNew = new Stock();
                    stockNew.setCount(product.getCount());
                    stockNew.setGrocery(finalPurchase.getGrocery());
                    stockNew.setProduct(product.getProduct());
                    stockRepository.save(stockNew);
                });
            }else{ // if stock has data update data and add count
                products.stream().forEach(product ->{
                            boolean flag = false;
                            for (Stock stock : stocks) {
                                if(stock.getProduct().getId() == product.getProduct().getId()){
                                    stock.setCount(stock.getCount().add(product.getCount()));
                                    flag = true;
                                }
                            }
                            if(flag == false){
                                Stock stockNew = new Stock();
                                stockNew.setCount(product.getCount());
                                stockNew.setGrocery(finalPurchase.getGrocery());
                                stockNew.setProduct(product.getProduct());
                                stockRepository.save(stockNew);
                            }
                        }
                );
            }

            for (PurchaseProduct purchaseProduct: products) {
                purchaseProductRepository.save(purchaseProduct);
            }
        }
        LOGGER.info("save method triggered");
        return purchaseMapper.toDto(purchase);
    }
    /**
     * Get all the purchase.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PurchaseDTO> findAll() {
        LOGGER.info("findAll method triggered");
        return purchaseRepository.findAll()
                .stream()
                .map(purchaseMapper::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }
    /**
     * Get one purchase by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PurchaseDTO> findOne(Long id) {
        LOGGER.info("findOne method triggered");
        return purchaseRepository.findById(id).map(purchaseMapper::toDto);
    }
    /**
     * Delete the purchase by id.
     *
     * @param id the id of the entity.
     */
    public Map<String, Boolean> delete(Long id) {

        Map<String, Boolean> map = new HashMap<>();
        if(purchaseRepository.existsById(id)){
            purchaseProductRepository.deleteByPurchaseId(id);
            purchaseRepository.deleteById(id);
            map.put("Purchase deleted successfully", true);
        }else{
            map.put("id not found", false);
            LOGGER.error("id not found");
        }
        LOGGER.info("delete method triggered");
        return map;
    }
}
