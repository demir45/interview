package tr.com.kolaysoft.manav.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import tr.com.kolaysoft.manav.domain.Purchase;
import tr.com.kolaysoft.manav.domain.PurchaseProduct;
import tr.com.kolaysoft.manav.service.dto.ProductSalePurchaseDTO;
import tr.com.kolaysoft.manav.service.dto.PurchaseDTO;

@Mapper(componentModel = "spring", uses = { GroceryMapper.class })
public interface PurchaseMapper extends EntityMapper<PurchaseDTO, Purchase> {

    @Mapping(target = "products", ignore = true)
    @Mapping(target = "removeProduct", ignore = true)
    Purchase toEntity(PurchaseDTO dto);

    @Mapping(target = "grocery", source = "grocery", qualifiedByName = "id")
    PurchaseDTO toDto(Purchase p);

    @Mapping(target = "productId", source = "product.id")
    ProductSalePurchaseDTO map(PurchaseProduct purchaseProduct);

}
