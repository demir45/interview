package tr.com.kolaysoft.manav.service.mapper;

import org.mapstruct.Mapper;
import tr.com.kolaysoft.manav.domain.Stock;
import tr.com.kolaysoft.manav.service.dto.StockDTO;

@Mapper(componentModel = "spring")
public interface StockMapper extends EntityMapper<StockDTO, Stock> {





}
