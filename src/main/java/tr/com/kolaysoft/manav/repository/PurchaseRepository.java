package tr.com.kolaysoft.manav.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tr.com.kolaysoft.manav.domain.Purchase;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Long> {

}
