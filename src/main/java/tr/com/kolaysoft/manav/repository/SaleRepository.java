package tr.com.kolaysoft.manav.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tr.com.kolaysoft.manav.domain.Sale;

import java.util.List;

/**
 * Spring Data SQL repository for the Sale entity.
 */
@Repository
public interface SaleRepository extends JpaRepository<Sale, Long> {

    List<Sale> findByGroceryId(Long groceryId);
}
