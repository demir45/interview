package tr.com.kolaysoft.manav.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tr.com.kolaysoft.manav.domain.Stock;

import java.util.Set;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {

    Set<Stock> findByGroceryId(Long groceryId);
    Stock findByGrocery_Id(Long groceryId);
void deleteById(Long id);

//List<Stock>
}
