package tr.com.kolaysoft.manav.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tr.com.kolaysoft.manav.domain.PurchaseProduct;
import tr.com.kolaysoft.manav.domain.PurchaseProductId;

@Repository
public interface PurchaseProductRepository extends JpaRepository<PurchaseProduct, PurchaseProductId> {

    void deleteByPurchaseId(Long purchaseId);
}
