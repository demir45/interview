package tr.com.kolaysoft.manav.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tr.com.kolaysoft.manav.domain.SaleProduct;
import tr.com.kolaysoft.manav.domain.SaleProductId;

import java.util.List;

@Repository
public interface SaleProductRepository extends JpaRepository<SaleProduct, SaleProductId> {
    List<SaleProduct> findById(Long id);

    List<SaleProduct> findBySaleId(Long id);
    void deleteBySaleId(Long saleId);
}
